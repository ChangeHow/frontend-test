(function() {
  // Figure out the path
  const getEndpoint = async () => {
    if (window._dam_endpoint) {
      return window._dam_endpoint;
    }

    const knownPaths = [
      "/a/dam",
      "/apps/digital-assets-manager",
    ];

    let endpoint = false;
    for (let path of knownPaths) {
      const r = await fetch(path, {
        method: 'HEAD',
      });

      console.log('goign to check path', path);
      if (r.status === 200) {
        endpoint = path;
        break;
      }
    }

    console.log('the path', endpoint);
    return endpoint;
  };

  const loadData = async () => {
    return {
      entries: [
        {
          productName: "t shrt 1",
          orderId: "123",
          type: "code",
          code: "1233h1jk2j123"
        },
        {
          productName: "Steering wheel",
          orderId: "423",
          type: "file",
          downloadLink: "https://www.example.com"
        }
      ],
      languages: { "productId": "Product Id", "type": "Type", "productName": "Product Name1", "digitalAsset": "Codes/Download link", "quantity": "Quantity1", "codes": "Codes1", "downloadLink": "Download Link1", "emailSubject_decline-claim": "Your claim has been declined", "emailSubject_claim-request": "Digital Asset Manager - There is a new claim request" },
    };
    const endpoint = await getEndpoint();
    if (window._dam_customer_id && endpoint) {
      try {
        const data = {};

        const res = await fetch(endpoint, {
          method: "POST",
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            customerId: window._dam_customer_id,
          }),
        });

        data.entries = await res.json();

        const langRes = await fetch(endpoint, {
          method: "POST",
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            action: 'languages',
          }),
        });

        data.languages = await langRes.json();

        return data;
      } catch (error) {
        throw new Error('Sorry somethng wrong while fetching data, Please refresh the page later');
      }
    } else {
      throw new Error('Please login to see your digital assets');
    }
  };

  const renderEntries = (entries, languages) => {
    const entryContent = (data) => {
      if (data.type === 'code') {
        return data.code;
      } else {
        return `<a target="_blank" href="${data.downloadLink}">Download file</a>`;
      }
    }

    const trs = entries.map((data, index) => `
          <tr className="dam-entry">
              <td>${data.orderId}</td>
              <td>${data.productName}</td>
              <td>${data.type}</td>
              <td>${entryContent(data)}</td>
          </tr>
    `);

    return `
      <table>
          <thead>
              <tr>
                  <th>${languages.productId}</th>
                  <th>${languages.productName}</th>
                  <th>${languages.type}</th>
                  <th>${languages.digitalAsset}</th>
              </tr>
          </thead>
          <tbody>
            ${trs.join('')}
          </tbody>
      </table>
    `
  };

  const getContent = async () => {
    try {
      // Load the data
      const data = await loadData();
      console.log('_dam data', data);

      let content = `<p>Sorry no orders with digital asset yet</p>`;
      if (data.entries.length > 0) {
        content = renderEntries(data.entries, data.languages);
      }

      return content;
    } catch (error) {
      console.error('_dam', error);
      return error
    }
  };

  const App = async () => {
    console.log('_dam going to load data');

    try {
      display('loading');
      const content = await getContent();

      display(content);
    } catch (error) {
      display(`
        <p>Sorry something went wrong, please refresh the page later</p>
        <p>${error}</p>
      `);
    }
  };

  const rootEl = document.getElementById('_dam_root');
  const display = (content) => rootEl.innerHTML = content;
  App();
})();
